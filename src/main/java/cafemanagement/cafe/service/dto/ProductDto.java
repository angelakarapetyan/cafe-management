package cafemanagement.cafe.service.dto;

import java.util.List;

public class ProductDto {

    private Long id;
    private String name;
    private Double price;
    private List<ProductInOrderDto> productInOrderEntities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<ProductInOrderDto> getProductInOrderEntities() {
        return productInOrderEntities;
    }

    public void setProductInOrderEntities(List<ProductInOrderDto> productInOrderEntities) {
        this.productInOrderEntities = productInOrderEntities;
    }
}
