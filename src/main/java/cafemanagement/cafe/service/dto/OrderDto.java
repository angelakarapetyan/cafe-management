package cafemanagement.cafe.service.dto;

import java.util.List;

public class OrderDto {

    public enum Status{
        OPEN,
        CLOSED,
        CANCELLED
    }

    private Long id;

    private Status status;

    private List<ProductInOrderDto> productInOrders;

    private TableDto table;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<ProductInOrderDto> getProductInOrders() {
        return productInOrders;
    }

    public void setProductInOrders(List<ProductInOrderDto> productInOrders) {
        this.productInOrders = productInOrders;
    }

    public TableDto getTable() {
        return table;
    }

    public void setTable(TableDto table) {
        this.table = table;
    }
}
