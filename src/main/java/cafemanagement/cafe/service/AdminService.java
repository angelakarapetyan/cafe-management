package cafemanagement.cafe.service;


import cafemanagement.cafe.persistence.entity.UserEntity;
import cafemanagement.cafe.persistence.repository.UserRepository;
import cafemanagement.cafe.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AdminService implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AdminService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        UserEntity manager = userRepository.findByUsername("User");
        if (manager == null) {
            manager = new UserEntity();
            manager.setName("manager");
            manager.setSurname("manager");
            manager.setUsername("User");
            manager.setRole(UserDto.Role.MANAGER.name());
            manager.setPassword(passwordEncoder.encode("Welcome555"));

            userRepository.save(manager);
        }
    }
}
