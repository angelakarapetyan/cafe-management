package cafemanagement.cafe.service;

import cafemanagement.cafe.persistence.entity.OrderEntity;
import cafemanagement.cafe.persistence.entity.TableEntity;
import cafemanagement.cafe.persistence.repository.OrderRepository;
import cafemanagement.cafe.service.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    private final TableService tableService;
    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(TableService tableService, OrderRepository orderRepository) {
        this.tableService = tableService;
        this.orderRepository = orderRepository;
    }

    public void createOrder(Long tableId) {
        TableEntity table = tableService.findById(tableId);
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setTable(table);
        orderEntity.setStatus(OrderDto.Status.OPEN.name());

        orderRepository.save(orderEntity);
    }
}
