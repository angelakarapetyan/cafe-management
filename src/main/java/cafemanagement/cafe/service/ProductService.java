package cafemanagement.cafe.service;

import cafemanagement.cafe.persistence.entity.ProductEntity;
import cafemanagement.cafe.persistence.entity.UserEntity;
import cafemanagement.cafe.persistence.repository.ProductRepository;
import cafemanagement.cafe.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void createProduct(ProductEntity productEntity) {
        productRepository.save(productEntity);
    }

    public List<ProductEntity> getProducts(){
       return productRepository.findAll();
    }
}
