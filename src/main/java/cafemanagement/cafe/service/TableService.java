package cafemanagement.cafe.service;

import cafemanagement.cafe.persistence.entity.TableEntity;
import cafemanagement.cafe.persistence.entity.UserEntity;
import cafemanagement.cafe.persistence.repository.TableRepository;
import cafemanagement.cafe.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TableService {

    private final TableRepository tableRepository;
    private final UserRepository userRepository;

    @Autowired
    public TableService(TableRepository tableRepository,
                        UserRepository userRepository) {
        this.tableRepository = tableRepository;
        this.userRepository = userRepository;
    }

    public void assignTableToWaiter(Long tableId, Long waiterId) {
        TableEntity table = tableRepository.findById(tableId).orElseThrow(() -> new EntityNotFoundException("table"));
        UserEntity waiter = userRepository.findById(waiterId).orElseThrow(() -> new EntityNotFoundException("user"));
        table.setUser(waiter);
        tableRepository.save(table);
    }

    public void createTable(TableEntity tableEntity){
        tableRepository.save(tableEntity);
    }

    public List<TableEntity> getTablesWithoutWaiters(){
        return tableRepository.findAllWithoutWaiter();
    }

    public List<TableEntity> getTablesWithoutOrder(){
        return tableRepository.findAllWithoutOrder();
    }

    public List<TableEntity> getUserTables(UserEntity userEntity){
        return tableRepository.findAllByUser(userEntity);
    }

    public TableEntity findById(Long tableId){
        TableEntity table = tableRepository.findById(tableId).orElseThrow(() -> new EntityNotFoundException("table"));

        return table;
    }
    public List<TableEntity> getTables(){
        return tableRepository.findAll();
    }
}
