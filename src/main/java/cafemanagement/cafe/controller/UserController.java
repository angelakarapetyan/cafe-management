package cafemanagement.cafe.controller;

import cafemanagement.cafe.persistence.entity.UserEntity;
import cafemanagement.cafe.service.UserService;
import cafemanagement.cafe.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String createUser(Model model) {
        List<UserEntity> users = userService.getUsers();
        model.addAttribute("users", users);
        model.addAttribute(new UserEntity());

        return "users";
    }

    @PostMapping
    public String createUser(@ModelAttribute UserEntity userEntity) {
        userService.createUser(userEntity);

        return "redirect:/users";
    }
}
