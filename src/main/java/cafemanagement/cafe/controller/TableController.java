package cafemanagement.cafe.controller;

import cafemanagement.cafe.persistence.entity.TableEntity;
import cafemanagement.cafe.persistence.entity.UserEntity;
import cafemanagement.cafe.security.CurrentUser;
import cafemanagement.cafe.service.TableService;
import cafemanagement.cafe.service.UserService;
import cafemanagement.cafe.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/tables")
public class TableController {

    private final TableService tableService;
    private final UserService userService;

    @Autowired
    public TableController(UserService userRepository,
                           TableService tableService) {
        this.userService = userRepository;
        this.tableService = tableService;
    }

    @GetMapping
    public String createTable(Model model) {
        List<TableEntity> tables = tableService.getTables();
        model.addAttribute(new TableEntity());
        model.addAttribute("tables", tables);
        return "tables";
    }

    @PostMapping
    public String createTable(@ModelAttribute TableEntity tableEntity) {

        tableService.createTable(tableEntity);
        return "redirect:/tables";
    }

    @GetMapping("/to_waiters")
    public String assignTableToWaiter(Model model) {

        List<UserEntity> waiters = userService.getUsersByRole(UserDto.Role.WAITER.name());
        List<TableEntity> tables = tableService.getTablesWithoutWaiters();
        model.addAttribute("waiters", waiters);
        model.addAttribute("tables", tables);

        return "assignTable";
    }

    @PostMapping("/to_waiters")
    public String assignTableToWaiter(@RequestParam("waiterId") Long waiterId,
                                      @RequestParam("tableId") Long tableId) {

       tableService.assignTableToWaiter(tableId, waiterId);

        return "redirect:/tables/to_waiters";
    }

    @GetMapping("/user")
    public String getUserTables(@AuthenticationPrincipal CurrentUser user, Model model){
        List<TableEntity> userTables = tableService.getUserTables(user.getUser());
        model.addAttribute("tables", userTables);

        return "tables";
    }
}
