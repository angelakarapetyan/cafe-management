package cafemanagement.cafe.controller;

import cafemanagement.cafe.persistence.entity.ProductEntity;
import cafemanagement.cafe.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public String createProduct(Model model) {
        List<ProductEntity> products = productService.getProducts();
        model.addAttribute(new ProductEntity());
        model.addAttribute("products", products);

        return "products";
    }

    @PostMapping
    public String createProduct(@ModelAttribute ProductEntity productEntity) {

        productService.createProduct(productEntity);

        return "redirect:/products";
    }
}
