package cafemanagement.cafe.controller;

import cafemanagement.cafe.persistence.entity.TableEntity;
import cafemanagement.cafe.service.OrderService;
import cafemanagement.cafe.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;
    private final TableService tableService;

    @Autowired
    public OrderController(OrderService orderService, TableService tableService){
        this.orderService = orderService;
        this.tableService = tableService;
    }

    @GetMapping
    public String createOrder(Model model) {
        List<TableEntity> tablesWithoutOrder = tableService.getTablesWithoutOrder();
        model.addAttribute("tables", tablesWithoutOrder);

        return "order";
    }

    @PostMapping
    public String createOrder(@RequestParam("tableId") Long tableId) {
        orderService.createOrder(tableId);

        return "order";
    }
}
