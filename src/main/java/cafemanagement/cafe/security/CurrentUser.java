package cafemanagement.cafe.security;

import cafemanagement.cafe.persistence.entity.UserEntity;
import org.springframework.security.core.authority.AuthorityUtils;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private UserEntity user;

    public CurrentUser(UserEntity user) {
        super(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRole()));
        this.user = user;
    }

    public UserEntity getUser() {
        return user;
    }
}
