package cafemanagement.cafe.persistence.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "orders",  uniqueConstraints = { @UniqueConstraint( columnNames = { "status", "table_id" } ) } )
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private String status;

    @OneToMany(mappedBy = "order", targetEntity = ProductInOrderEntity.class)
    private List<ProductInOrderEntity> productInOrders;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "table_id", referencedColumnName = "id")
    private TableEntity table;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProductInOrderEntity> getProductInOrders() {
        return productInOrders;
    }

    public void setProductInOrders(List<ProductInOrderEntity> productInOrders) {
        this.productInOrders = productInOrders;
    }

    public TableEntity getTable() {
        return table;
    }

    public void setTable(TableEntity table) {
        this.table = table;
    }
}
