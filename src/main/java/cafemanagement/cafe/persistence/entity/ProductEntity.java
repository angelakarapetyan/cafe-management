package cafemanagement.cafe.persistence.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "products" )
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private Double price;

    @OneToMany(mappedBy = "product", targetEntity = ProductInOrderEntity.class)
    private List<ProductInOrderEntity> productInOrders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<ProductInOrderEntity> getProductInOrders() {
        return productInOrders;
    }

    public void setProductInOrders(List<ProductInOrderEntity> productInOrders) {
        this.productInOrders = productInOrders;
    }
}
