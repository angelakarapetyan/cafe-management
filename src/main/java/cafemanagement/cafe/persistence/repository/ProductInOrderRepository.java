package cafemanagement.cafe.persistence.repository;

import cafemanagement.cafe.persistence.entity.ProductInOrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInOrderRepository extends JpaRepository<ProductInOrderEntity, Long> {
}
