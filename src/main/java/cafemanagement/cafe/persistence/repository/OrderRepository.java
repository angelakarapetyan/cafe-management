package cafemanagement.cafe.persistence.repository;

import cafemanagement.cafe.persistence.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
}
