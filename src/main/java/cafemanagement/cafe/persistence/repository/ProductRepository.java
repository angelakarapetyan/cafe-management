package cafemanagement.cafe.persistence.repository;

import cafemanagement.cafe.persistence.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
}
