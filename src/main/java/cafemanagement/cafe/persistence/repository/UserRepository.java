package cafemanagement.cafe.persistence.repository;

import cafemanagement.cafe.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByUsername(String username);
    List<UserEntity> findAllByRole(String role);
}
