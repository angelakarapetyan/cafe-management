package cafemanagement.cafe.persistence.repository;

import cafemanagement.cafe.persistence.entity.TableEntity;
import cafemanagement.cafe.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TableRepository extends JpaRepository<TableEntity, Long> {

    @Query(value = "SELECT t FROM TableEntity t WHERE t.user IS NULL")
    List<TableEntity> findAllWithoutWaiter();

    @Query(value = "SELECT t FROM TableEntity t LEFT JOIN t.order o WHERE o IS NULL")
    List<TableEntity> findAllWithoutOrder();

    List<TableEntity> findAllByUser(UserEntity user);
}
